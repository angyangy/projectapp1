FROM python:latest
COPY flask ./app
WORKDIR app
RUN python3 -m venv venv
RUN . venv/bin/activate
RUN pip install -e .
RUN export FLASK_APP=flaskr
RUN export FLASK_ENV=development
CMD flask run


